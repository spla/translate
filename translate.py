import sys
import os
import os.path
import re
from datetime import datetime, timedelta
from mastodon import Mastodon
import psycopg2
import subprocess
import requests
import urllib

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def unescape(s):
    s = s.replace("&apos;", "'")
    return s

def get_bot_id():

    ###################################################################################################################################
    # get bot_id from bot's username param

    try:

        conn = None

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select id from accounts where username = (%s) and domain is null", (bot_username,))

        row = cur.fetchone()

        if row != None:

            bot_id = row[0]

        cur.close()

        return bot_id

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def get_user_domain(account_id):

    try:

        conn = None

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select username, domain from accounts where id=(%s)", (account_id,))

        row = cur.fetchone()

        if row != None:

            username = row[0]

            domain = row[1]

        cur.close()

        return (username, domain)

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def get_mentions():

    account_id_lst = []

    status_id_lst = []

    conn = None

    try:

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        select_query = "select account_id, id from statuses where created_at + interval '120 minutes' > now() - interval '5 minutes'"
        select_query += " and id=any (select status_id from mentions where account_id=(%s)) order by created_at asc"

        cur.execute(select_query, (str(bot_id),))

        rows = cur.fetchall()

        for row in rows:

            replied = check_replies(row[1])

            if not replied:

                account_id_lst.append(row[0])

                status_id_lst.append(row[1])

        cur.close()

        return (account_id_lst, status_id_lst)

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def get_notification_data(status_id):

    conn = None

    try:

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        select_query = "select text, visibility from statuses where id=(%s)"

        cur.execute(select_query, (status_id,))

        row = cur.fetchone()

        text = row[0]

        if row[1] == 0:
            visibility = 'public'
        elif row[1]  == 1:
            visibility = 'unlisted'
        elif row[1]  == 2:
            visibility = 'private'
        elif row[1]  == 3:
            visibility = 'direct'

        cur.close()

        return (text, visibility)

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def update_replies(status_id, username, now):

    post_id = status_id

    try:

        conn = None

        conn = psycopg2.connect(database = translate_db, user = translate_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        insert_sql = "insert into botreplies(status_id, query_user, status_created_at) values(%s, %s, %s) ON CONFLICT DO NOTHING"

        cur.execute(insert_sql, (post_id, username, now))

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        sys.exit(error)

    finally:

        if conn is not None:

            conn.close()

def check_replies(status_id):

    post_id = status_id

    replied = False

    try:

        conn = None

        conn = psycopg2.connect(database = translate_db, user = translate_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select status_id from botreplies where status_id=(%s)", (post_id,))

        row = cur.fetchone()

        if row != None:

            replied = True

        else:

            replied = False

        cur.close()

        return replied

    except (Exception, psycopg2.DatabaseError) as error:

        sys.exit(error)

    finally:

        if conn is not None:

            conn.close()

def replying():

    reply = False

    content = cleanhtml(text)
    content = unescape(content)

    try:

        start = content.index("@")
        end = content.index(" ")
        if len(content) > end:

            content = content[0: start:] + content[end +1::]

        neteja = content.count('@')

        i = 0
        while i < neteja :

            start = content.rfind("@")
            end = len(content)
            content = content[0: start:] + content[end +1::]
            i += 1

        question = content.lower()

        query_word = question
        query_word_length = len(query_word)

        if query_word[:2]  == 'ca' or query_word[:2] == 'en':

            reply = True

        return (reply, query_word)

    except ValueError as v_error:

        print(v_error)

def translate(key_word):

    if key_word == 'ca':

        url = urllib.parse.quote_plus('en' + '/' + 'ca' + '/' + word_to_translate)

    elif key_word == 'en':

        url = urllib.parse.quote_plus('ca' + '/' + 'en' + '/' + word_to_translate)

    res = requests.get('https://' + lingva_host + '/api/v1/' + url)
    translated_word = res.json()['translation']

    return translated_word

def mastodon():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)
    bot_username = get_parameter("bot_username", config_filepath)
    lingva_host = get_parameter("lingva_host", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return (mastodon, mastodon_hostname, bot_username, lingva_host)

def db_config():

    # Load db configuration from config file
    config_filepath = "config/db_config.txt"
    mastodon_db = get_parameter("mastodon_db", config_filepath)
    mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
    translate_db = get_parameter("translate_db", config_filepath)
    translate_db_user = get_parameter("translate_db_user", config_filepath)

    return (mastodon_db, mastodon_db_user, translate_db, translate_db_user)

def get_parameter( parameter, file_path ):

    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def usage():

    print('usage: python ' + sys.argv[0] + ' --play' + ' --en')

###############################################################################
# main

if __name__ == '__main__':

    # usage modes

    if len(sys.argv) == 1:

        usage()

    elif len(sys.argv) >= 2:

        if sys.argv[1] == '--play':

            bot_lang = ''

            if len(sys.argv) == 3:

                if sys.argv[2] == '--ca':

                    bot_lang = 'ca'

                if sys.argv[2] == '--es':

                    bot_lang = 'es'

                if sys.argv[2] == '--fr':

                    bot_lang = 'fr'

                elif sys.argv[2] == '--en':

                    bot_lang = 'en'

            elif len(sys.argv) == 2:

                bot_lang = 'ca'

            if not bot_lang in ['ca', 'es', 'fr', 'en']:

                print("\nOnly 'ca', 'es', 'fr' and 'en' languages are supported.\n")
                sys.exit(0)

            mastodon, mastodon_hostname, bot_username, lingva_host = mastodon()

            mastodon_db, mastodon_db_user, translate_db, translate_db_user = db_config()

            now = datetime.now()

            bot_id = get_bot_id()

            account_id_lst, status_id_lst = get_mentions()

            if len(account_id_lst) == 0:

                print('No mentions')
                sys.exit(0)

            i = 0

            while i < len(account_id_lst):

                account_id = account_id_lst[i]

                username, domain = get_user_domain(account_id)

                status_id = status_id_lst[i]

                text, visibility = get_notification_data(status_id)

                status_id = status_id_lst[i]

                replied = check_replies(status_id)

                if replied == True:

                    i += 1

                    continue

                reply, query_word = replying()

                if reply == True:

                    if query_word[:2] == 'ca' or query_word[:2] == 'en':

                        key_word = query_word[:2]

                        word_to_translate = query_word[3:] #.replace(' ', '')

                        if word_to_translate != '':

                            translated_word = translate(key_word)

                            if domain != None:

                                toot_text = '@' + username + '@' + domain + ", traducció de '" + word_to_translate + "' :\n\n"

                            else:

                                toot_text = '@' + username + ", traducció de '" + word_to_translate + "' :\n\n"

                            toot_text += translated_word + '\n'

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                            update_replies(status_id, username, now)

                else:

                    update_replies(status_id, username, now)

        else:

            usage()
