# Mastodon translate
Translate is a bot written with Python that translate any string from catalan to english and english to catalan languages.  
It get the translation from [Lingva](https://python-chess.readthedocs.io/en/latest/) REST API, a really good [TheDavidDelta](https://mastodon.social/@TheDavidDelta) project.  
Any user from the fediverse can ask the bot to translate anything from/to supported languages.  

### How to

Translating from catalan to english:

@your_bot_username en your_catalan_string_here

Translating from english to catalan:

@your_bot_username ca your_english_string_here

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon's bot account
-   [Mastodon](https://joinmastodon.org) server admin access

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed Python libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed tables in it.  

3. Run `python setup.py` to get your Mastodon's bot account tokens.

4. Use your favourite scheduling method to set `python translate.py` to run regularly.

